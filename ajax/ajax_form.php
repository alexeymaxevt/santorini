<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$iblockID = 8;

$name = htmlspecialcharsbx($request->getPost('name'));
$email = htmlspecialcharsbx($request->getPost('email'));
$message = htmlspecialcharsbx($request->getPost('message'));
$politics = htmlspecialcharsbx($request->getPost('politics'));

$arResult = array(
    'hasError' => true,
    'msg' => "Ошибка отправки сообщения",
);
$error_fields = '';
$name_error = false;
$email_error = false;
$message_error = false;
$politics_error = false;
//Убираем проверку elseif, пишем так, чтобы ВСЕ ошибки сохранились в массив
//Я немного переделал, но конкретно, как реализовать это через массив я не совсем понял
//!$request->isPost() - это зачем?
if (  !$request->isPost()
    || !($name && $email && $message && $politics == 'on')
    || !Loader::includeModule('iblock')
) {

    if (!$name) {
        $name_error = true;
    }
    elseif (!$email) {
        $email_error = true;
    }
    elseif (!$message) {
        $message_error = true;
    }
    elseif ($politics !== 'on') {
        $politics_error = true;
    }
}

if ($name_error || $email_error || $message_error || $politics_error) {
    $error_fields = 'Ошибка: Не заполнены следующие поля'.PHP_EOL;
    if ($name_error) {
        $error_fields = $error_fields.'ИМЯ'.PHP_EOL;
        $name_error = false;
    }
    if ($email_error) {
        $error_fields = $error_fields.'E-MAIL'.PHP_EOL;
        $email_error = false;
    }
    if ($message_error) {
        $error_fields = $error_fields.'СООБЩЕНИЕ'.PHP_EOL;
        $message_error = false;
    }
    if ($politics_error) {
        $error_fields = $error_fields.'ПОЛИТИКА'.PHP_EOL;
        $politics_error = false;
    }
    $arResult['msg'] = $error_fields;
}

$arProps = array(
    'EMAIL' => $email,
);

$arFields = array(
    'IBLOCK_SECTION_ID' => false,
    'IBLOCK_ID' => $iblockID,
    'ACTIVE' => 'Y',
    'NAME' => $name,
    'DETAIL_TEXT' => $message,
    'PROPERTY_VALUES' => $arProps
);
CModule::IncludeModule("iblock");
$el = new \CIBlockElement();
$itemId = $el->Add($arFields);
//Добавляем проверку на успешное добавление
//Если ошибка, то логируем ее
if (!$itemId) {
    $arResult['msg'] = "Ошибка: запись не добавлена в Инфоблок";
    error_log('Новая запись не добавлена в инфоблок Напишите нам', 0);
}
$formID = 1;

$arValues = array(
    "form_text_1" => $name,
    "form_email_2" => $email,
    "form_textarea_3" => $message
);

CModule::IncludeModule("form");
$formEl = new \CFormResult();
$formItem = $formEl->Add($formID, $arValues);
//Добавляем проверку на успешное добавление
//Если ошибка, то логируем ее
if (!$formItem) {
    $arResult['msg'] = "Ошибка: запись не добавлена в Веб-формы";
    error_log('Новая запись не добавлена в Веб-формы', 0);
}

if(!empty($itemId) && !empty($formItem)) {
    $arResult = array(
        'hasError' => false,
        'msg' => "Спасибо! Ваше обращение отправлено.",
    );
}

$arEventSend = array(
    "NAME"      => $name,
    "EMAIL"      => $email,
    "MESSAGE"     => $message,
);

CEvent::Send("FEEDBACK_SEND", array(SITE_ID), $arEventSend);

echo json_encode($arResult);