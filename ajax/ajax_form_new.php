<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
CModule::IncludeModule("iblock");
CModule::IncludeModule("form");

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$iblockID = 8;

$name = htmlspecialcharsbx($request->getPost('name'));
$email = htmlspecialcharsbx($request->getPost('email'));
$message = htmlspecialcharsbx($request->getPost('message'));
$politics = htmlspecialcharsbx($request->getPost('politics'));

$hasError = false;
$arResult = $errors = array();

if (!$name) {
    $hasError = true;
    $errors[] = 'Некорректно заполнено Имя';
}
if (!$email) {
    $hasError = true;
    $errors[] = 'Некорректно заполнен Email';
}
if (!$message) {
    $hasError = true;
    $errors[] = 'Некорректно заполнено Сообщение';
}
if ($politics !== 'on') {
    $hasError = true;
    $errors[] = 'Не заполнена политика';
}
//Если нет ошибок
if (!$hasError) {

    $arProps = array(
        'EMAIL' => $email,
    );

    $arFields = array(
        'IBLOCK_SECTION_ID' => false,
        'IBLOCK_ID' => $iblockID,
        'ACTIVE' => 'Y',
        'NAME' => $name,
        'DETAIL_TEXT' => $message,
        'PROPERTY_VALUES' => $arProps
    );

    $el = new \CIBlockElement();
    $itemId = $el->Add($arFields);
    //Добавляем проверку на успешное добавление
    //Если ошибка, то логируем ее
    if (!$itemId) {
        $hasError = true;
        $errors[] = "Ошибка: попробуйте, пожалуйста, позже";
        $arFieldsErrors = array(
            'error' => 'Ошибка при добавлении в инфоблок',
            'arFields' => $arFields
        );
        Bitrix\Main\Diag\Debug::writeToFile($arFieldsErrors,"","logrealfields.log");
       // error_log('Новая запись не добавлена в инфоблок Напишите нам', 0);
    }
    $formID = 1;

    $arValues = array(
        "form_text_1" => $name,
        "form_email_2" => $email,
        "form_textarea_3" => $message
    );

    $formEl = new \CFormResult();
    $formItem = $formEl->Add($formID, $arValues);
    //Добавляем проверку на успешное добавление
    //Если ошибка, то логируем ее
    if (!$formItem) {
        $hasError = true;
        $errors[] = "Ошибка: попробуйте, пожалуйста, позже";
        $arFieldsErrors = array(
            'error' => 'Ошибка: запись не добавлена в Веб-формы',
            'arValues' => $arValues,
            'formID' => $formID
        );
        Bitrix\Main\Diag\Debug::writeToFile($arFieldsErrors,"","logrealfields.log");
        //error_log('Новая запись не добавлена в Веб-формы', 0);
    }

    $arEventSend = array(
        "NAME" => $name,
        "EMAIL" => $email,
        "MESSAGE" => $message,
    );

    CEvent::Send("FEEDBACK_SEND", array(SITE_ID), $arEventSend);
}

$arResult = array(
    'hasError' => $hasError,
);
if (!$hasError)
    $arResult['msg'][] = "Спасибо! Ваше обращение отправлено";
 else
     $arResult['msg'] = $errors;

//msg на фронте нужно распорсить все ошибки и вывести
//Сейчас формат $arResult['msg'] = array
echo json_encode($arResult);