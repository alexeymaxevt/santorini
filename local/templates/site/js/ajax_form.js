$(function () {
    $(document).on('submit', '.ajax_form', function() {
        //Эта строчка для чего? Тут событие submit, она тут не нужна
        /*e.preventDefault();*/
        /*var $this = $(this);*/
        var form = $(this);
        //Событие submit, $(this) и есть форма, это лишнее
        /*var $form = $this.closest('form');*/
        /*var formUrl = $form.attr('action');*/
        var formUrl = form.attr('action');
        //Название formdata это другое, переименовать нужно чтобы не путаться
        //для чего нужен символ $? Тут его не нужно ставить
        /*var $formdata = $form.serialize();*/

        $.ajax({
            url:formUrl,
            method: 'post',
            type: "POST",
            //Тут ерунда написана, работать не будет
            //У тебя уже есть необходимые данные в $formdata
            /*data: new FormData($formdata.get(0)),*/
            data: form.serialize(),
            // contentType: false,
            // cache: false,
            // processData:false,
            dataType:'json',
            success: function (res) {
                console.log('ok');
                console.log(res);
                if(res.msg) {
                    alert(res.msg);
                }

                if(!res.hasError) {
                    form.find('input, textarea').val('');
                }
            },
            error: function (p1, p2, p3) {
                console.log(p1);
                console.log(p2);
                console.log(p3);
            }
        });

        return false;
    });
});