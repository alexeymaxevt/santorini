$(function() {
    // Если нет поддержки тега <svg>
  if (!Modernizr.inlinesvg) {
    $('[data-png]').each(function(){
      var src = $(this).data('png');
      $(this).before($('<img src="' + src + '" alt="">')).remove();
    });
  }

  // Если нет поддержки картинки image.svg
  if (!Modernizr.svgasimg) {
    $('img[src $= ".svg"]').each(function(){
      var src = $(this).attr('src');
      $(this).attr('src', src.replace('.svg', '.png'));
    });
  }


  if ($('.main_block__container .h1').text() === 'Новости') {
    $('.main_block .img_wrap').each(function (index, element) {
      $(element).find('.img').addClass('opacity');
      var src = $(element).find('.img').attr('src');
      $(element).parallax({imageSrc: src, speed: 0.5});
    });
  } else {
    $('.img_wrap').not('.services .img_wrap').each(function (index, element) {
      $(element).find('.img').addClass('opacity');
      var src = $(element).find('.img').attr('src');
      $(element).parallax({imageSrc: src, speed: 0.8});
    });
  }
});