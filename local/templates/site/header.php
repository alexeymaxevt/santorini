<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>

    <?=$APPLICATION->ShowHead();?>
<!--    <meta charset="utf-8">-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <meta name="theme-color" content="#ccc"/>

<!--    <title>SANTORINI - Главная страница</title>-->
    <title><?$APPLICATION->ShowTitle()?></title>
    <?//Стили и скрипты подключаем через фунции битрикса D7 style?>
    <?
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/base.css");
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/base/vip.css");
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/tablet.css");
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/phablet.css");
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/phone.css");
    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/fonts.css");
//    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/slick-theme.css");
//    \Bitrix\Main\Page\Asset::getInstance()->addCss(SITE_TEMPLATE_PATH."/styles/css/slick.css");
    ?>

</head>

<body>
<?=$APPLICATION->ShowPanel()?>
<header class="header" id="header">
    <button class="btn_show_nav" id="btn_show_nav"><span class="line"></span></button>
    <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "top"
	),
	false
);?>
    <div class="logo">
        <?//Добавляем проверку на нахождение на главной странице, чтобы не было редиректа самого на себя?>
        <?if ($APPLICATION->GetCurPage(true) == SITE_DIR . "index.php") {?>
            <img class="link" src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="">
        <?} else {?>
            <a class="link" href="<?=SITE_DIR?>"></a>
        <?}?>
    </div>
</header>

<?if ($APPLICATION->GetProperty("MAIN_CLASS") == "Y") {?>
    <main class="main">
<?}
else {
?>
    <main class="main <?=$APPLICATION->ShowProperty("MAIN_CLASS")?>">
<?}?>
<?if ($APPLICATION->GetCurPage() == '/contacts/') {?>
    <div class="p_contacts">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="h2 big"><?echo GetMessage("CONTACTS")?></div>
                </div>
            </div>
        </div>

        <button class="btn_write_to_us" id="btn_write_to_us"><span class="text"><?echo GetMessage("WRITE_US")?></span></button>
        <div class="map" id="map" style="width: 100%; height: 690px"></div>
    </div>
<?}
else {
?>
    <button class="btn_write_to_us" id="btn_write_to_us"><span class="text"><?echo GetMessage("WRITE_US")?></span></button>
<?}?>
