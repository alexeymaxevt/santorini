<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="socials">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?if (!empty($arItem)) {?>
            <a class="link link_<?=$arItem["NAME"]?>" href="<?=$arItem["PROPERTIES"]["LINK"]["VALUE"]?>" target="_blank"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" data-bx-app-ex-src="#BXAPP0#" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="icon" data-bx-orig-src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"></a>
        <?}?>
    <?endforeach;?>
</div>