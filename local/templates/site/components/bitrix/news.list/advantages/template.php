<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="info_blocks d-flex flex-wrap">
                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <div class="info_block">
                        <div class="info_block__value"><? echo $arItem["NAME"] ?></div>
                        <? //Добавляем проверку на заполненность?>
                        <? if (!empty($arItem["PREVIEW_TEXT"])) { ?>
                            <div class="info_block__value_description"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                        <? } ?>
                        <? if (!empty($arItem["DETAIL_TEXT"])) { ?>
                            <div class="info_block__description">
                                <? //Добавляем проверку на заполненность?>
                                <? echo $arItem["DETAIL_TEXT"]; ?>
                            </div>
                        <? } ?>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
</div>