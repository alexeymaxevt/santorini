<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $phone_source = $arItem["PROPERTIES"]["PHONE"]["VALUE"];
    $phone_without_left_brackets = str_replace('(', '', $phone_source);
    $phone_without_right_brackets = str_replace(')', '', $phone_without_left_brackets);
    $phone_without_spaces = str_replace(' ', '', $phone_without_right_brackets);
    $phone = str_replace('-', '', $phone_without_spaces);
    ?>
    <? if (!empty($arItem["PROPERTIES"]["ADDRESS"]["VALUE"])) { ?>
        <div class="contact">
            <div class="contact__label">Наш адрес</div>
            <div class="contact__value"><?= $arItem["PROPERTIES"]["ADDRESS"]["VALUE"] ?></div>
        </div>
    <? } ?>
    <div class="contact">
        <div class="contact__label">E-Mail</div>
        <div class="contact__value">
            <? if (!empty($arItem["PROPERTIES"]["EMAIL_INFO"]["VALUE"])) { ?>
                <span class="value_row">информация — <a class="link" href="mailto:<?= $arItem["PROPERTIES"]["EMAIL_INFO"]["VALUE"] ?>"><?= $arItem["PROPERTIES"]["EMAIL_INFO"]["VALUE"] ?></a> </span>
            <? } ?>
            <? if (!empty($arItem["PROPERTIES"]["EMAIL_PR"]["VALUE"])) { ?>
                <span class="value_row">pr-реклама — <a class="link" href="mailto:<?= $arItem["PROPERTIES"]["EMAIL_PR"]["VALUE"] ?>"><?= $arItem["PROPERTIES"]["EMAIL_PR"]["VALUE"] ?></a></span>
            <? } ?>
        </div>
    </div>
    <? if (!empty($phone) && !empty($phone_source)) { ?>
        <div class="contact">
            <div class="contact__label">Телефон</div>
            <div class="contact__value"><a href="tel:<?= $phone ?>"><?= $phone_source ?></a></div>
        </div>
    <? } ?>
<? endforeach; ?>