<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="main_block">
        <div class="img_wrap">
            <img class="main_block__img img"
                 src="<?= $arItem["DISPLAY_PROPERTIES"]["MAIN_IMAGE"]["FILE_VALUE"]["SRC"] ?>" alt="">
        </div>
        <div class="main_block__container">
            <div class="h1"><?= $arItem["NAME"] ?></div>
            <div class="description">
                <?= $arItem["PROPERTIES"]["MAIN_HEAD"]["~VALUE"]["TEXT"] ?>
            </div>
        </div>
    </div>

    <div class="change_color_header" id="change_color_header"></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <div class="bg_waves" style="height: 575px; position: absolute; width: calc(100% - 40px);"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-1"></div>
            <div class="col-sm-6 col-lg-5">
                <h2 class="h2 big"><?= $arItem["PROPERTIES"]["HEAD"]["VALUE"] ?></h2>
                <p class="paragraf">
                    <?= $arItem["PROPERTIES"]["TEXT_1"]["~VALUE"]["TEXT"] ?>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-1">

            </div>
            <div class="col-sm-12 col-lg-10">
                <div class="img_slider_wrap">
                    <div class="img_slider_page" id="img_slider_page">01</div>
                    <div class="img_slider" id="img_slider">
                        <? foreach ($arItem["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"] as $image): ?>
                            <img src="<?= $image["SRC"] ?>" alt="" class="img">
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-1">

            </div>
            <div class="col-sm-7 col-lg-6 b_list">
                <div class="h2"><?= $arItem["PROPERTIES"]["HEAD_LIST"]["VALUE"] ?></div>
                <ul class="list">
                    <? foreach ($arItem["PROPERTIES"]["TEXT_2"]["VALUE"] as $list): ?>
                        <li class="list__item"><?= $list ?></li>
                    <? endforeach; ?>
                </ul>
            </div>
            <div class="col-sm-5 col-lg-4">
                <div class="img_wrap">
                    <img class="img" src="<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE_1"]["FILE_VALUE"]["SRC"] ?>" alt="">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-1">
            </div>
            <div class="col-sm-6 col-lg-5">
                <div class="img_wrap mb_none">
                    <img class="img" src="<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE_2"]["FILE_VALUE"]["SRC"] ?>" alt="">
                </div>
            </div>
            <div class="col-sm-6 col-lg-5">
                <div class="bg_waves" style="width: 110%; position: absolute; top: 0; right: 0; height: 100%;"></div>
            </div>
        </div>

        <div class="bg_waves mobile_only"></div>

        <? $APPLICATION->IncludeComponent(
            "bitrix:main.include",
            "",
            array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR . "include/site_capacity.php"
            )
        ); ?>
    </div>
<? endforeach; ?>
