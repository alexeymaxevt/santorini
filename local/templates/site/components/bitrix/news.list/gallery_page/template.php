<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="main_block">
        <? if (!empty($arItem["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"])) { ?>
            <div class="gallery_page_slider" id="gallery_page_slider">
                <? foreach ($arItem["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"] as $image): ?>
                    <img class="main_block__img" src="<?= $image["SRC"] ?>" alt="">
                <? endforeach; ?>
            </div>
        <? } ?>

        <button class="btn_gallery" id="btn_gallery" type="button">
            <div class="circles"></div>
            <div class="circles"></div>
            <div class="circles"></div>
        </button>

    </div>

    <div class="change_color_header" id="change_color_header"></div>

    <? if (!empty($arItem["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"])) { ?>
        <div class="gallery" id="gallery">
            <button class="btn_close" id="btn_close_gallery"></button>
            <? foreach ($arItem["DISPLAY_PROPERTIES"]["GALLERY"]["FILE_VALUE"] as $image): ?>
                <img src="<?= $image["SRC"] ?>" alt="" class="gallery__img img">
            <? endforeach; ?>
        </div>
    <? } ?>
<? endforeach; ?>