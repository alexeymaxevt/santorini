<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>
<div class="services">
    <div class="container">
        <div class="row align-items-end">
            <div class="col-sm-6">
                <div class="bg_waves"></div>
                <h2 class="mobile_only"><?=Loc::getMessage('NAME');?></h2>
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?if($arItem["PROPERTIES"]["DESKTOP_ONLY"]["VALUE"]=='' && $arItem["PROPERTIES"]["BLOCK_RIGHT"]["VALUE"]=='') {?>
                        <div class="cover service">
                            <div class="img_wrap">
                                <?//Добавляем проверку на заполненность и обрезку картинки?>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="img">
                            </div>
                            <?//Добавляем ссылку?>
                            <a href="" class="link"><?echo $arItem["NAME"]?></a>
                        </div>
                    <?} elseif($arItem["PROPERTIES"]["DESKTOP_ONLY"]["VALUE"]=='' && $arItem["PROPERTIES"]["BLOCK_RIGHT"]["VALUE"]=='Y') {?>
                        <div class="service block-right">
                            <div class="img_wrap">
                                <?//Добавляем проверку на заполненность и обрезку картинки?>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="img">
                            </div>
                            <?//Добавляем ссылку?>
                            <a href="" class="link"><?echo $arItem["NAME"]?></a>
                        </div>
                    <?}?>
                <?endforeach;?>
            </div>

            <div class="col-sm-6">
                <h2 class="desktop_only"><?=Loc::getMessage('NAME');?></h2>
                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?if($arItem["PROPERTIES"]["DESKTOP_ONLY"]["VALUE"]=='Y' && $arItem["PROPERTIES"]["BLOCK_RIGHT"]["VALUE"]=='') {?>
                        <div class="service">
                            <div class="img_wrap">
                                <?//Добавляем проверку на заполненность и обрезку картинки?>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>" class="img">
                            </div>
                            <?//Добавляем ссылку?>
                            <a href="" class="link"><?echo $arItem["NAME"]?></a>
                        </div>
                    <?}?>
                <?endforeach;?>
            </div>
        </div>
    </div>
</div>