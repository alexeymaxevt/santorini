<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);
?>
<div class="tariffs">
    <div class="container">
        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-10 d-flex flex-column">
                <div class="tariffs_cards">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>

                        <? if (!empty($arItem["PREVIEW_PICTURE"]) && !empty($arItem["PREVIEW_TEXT"])) { ?>
                            <div class="tariffs_card">
                                <? //Добавляем проверку на заполненность и обрезку картинки?>
                                <?
                                $renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => 1280, "height" => 700), BX_RESIZE_IMAGE_PROPORTIONAL, false);
                                ?>
                                <img src="<?= $renderImage["src"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                     class="tariffs_card__img img">
                                <div class="tariffs_card__name"><? echo $arItem["NAME"] ?></div>
                                <div class="tariffs_card__prices">
                                    <? //Добавляем проверку на заполненность?>
                                    <div class="div"><? echo $arItem["PREVIEW_TEXT"]; ?></div>
                                    <? if (!empty($arItem["DETAIL_TEXT"])) { ?>
                                        <div class="div"><? echo $arItem["DETAIL_TEXT"]; ?></div>
                                    <? } ?>
                                </div>
                            </div>
                        <? } ?>

                    <? endforeach; ?>

                </div>

                <? if (!empty($arResult["TEXT"])) { ?>
                    <div class="text">
                        <? //Все запросы должны быть в result_modifier?>
                        <?= $arResult["TEXT"] ?>
                    </div>
                <? } ?>

            </div>

            <div class="col-md-1 title_block">
                <div class="title_vert"><?= Loc::getMessage('NAME'); ?></div>
            </div>

        </div>
    </div>
</div>
