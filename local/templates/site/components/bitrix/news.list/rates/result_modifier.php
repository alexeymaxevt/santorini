<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$description = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "DESCRIPTION");
$arResult["TEXT"] = $description;
$this->__component->SetResultCacheKeys(['TEXT']);