<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main_slider">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-8">
                <div class="main_header_slider_wraper">
                    <div class="main_header_slider_page" id="main_header_slider_page">01</div>
                    <div class="main_header_slider" id="main_header_slider">
                        <? foreach ($arResult["ITEMS"] as $arItem): ?>
                            <? //Форматируем, чтобы красиво было и понятно что к чему относится?>
                            <div class="main_header_slider__slide slide_header">
                                <b><?= $arItem["NAME"] ?></b>
                                <?= $arItem["PREVIEW_TEXT"]; ?>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-3">
                <div class="main_text_slider" id="main_text_slider">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <? //Форматируем, чтобы красиво было и понятно что к чему относится?>
                        <div class="main_text_slider__slide text">
                            <?= $arItem["DETAIL_TEXT"]; ?>
                        </div>
                    <? endforeach; ?>
                </div>
                <div class="slider_page" id="main_text_slider_page">
                    01
                </div>
            </div>
            <div class="col-sm-6">
                <div class="bg_waves slide_waves"></div>
            </div>
        </div>
    </div>
</div>

<div class="main_slider_video" id="main_slider_video">
    <div class="main_slider_video__slide">
        <div class="hover"></div>
        <div class="video_wrap">
            <? foreach ($arResult["ITEMS"] as $arItem): ?>
                <? //Форматируем, чтобы красиво было и понятно что к чему относится?>
                <video class="video in_active" src="<?= $arItem["PROPERTIES"]["VIDEO"]["VALUE"]["path"] ?>" autoplay
                       loop muted></video>
            <? endforeach; ?>
        </div>
    </div>
</div>
