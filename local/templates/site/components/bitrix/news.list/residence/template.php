<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <div class="main_block">
        <? if (!empty($arItem["DISPLAY_PROPERTIES"]["MAIN_IMAGE"]["FILE_VALUE"]["SRC"])) { ?>
            <div class="img_wrap">
                <img class="main_block__img img" src="<?= $arItem["DISPLAY_PROPERTIES"]["MAIN_IMAGE"]["FILE_VALUE"]["SRC"] ?>" alt="">
            </div>
        <? } ?>
        <div class="main_block__container">
            <div class="h1"><?= $arItem["NAME"] ?></div>
            <? if (!empty($arItem["PROPERTIES"]["SUBTITLE"]["VALUE"])) { ?>
                <div class="description">
                    <?= $arItem["PROPERTIES"]["SUBTITLE"]["VALUE"] ?>
                </div>
            <? } ?>
        </div>
    </div>

    <div class="change_color_header" id="change_color_header"></div>

    <div class="container">
        <div class="row">
            <div class="col-sm-6"></div>
            <div class="col-sm-6">
                <div class="bg_waves" style="height: 575px; position: absolute; width: calc(100% - 40px);"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-1"></div>
            <? if (!empty($arItem["PROPERTIES"]["HEAD_1"]["VALUE"]) && !empty($arItem["PROPERTIES"]["TEXT_1"]["~VALUE"]["TEXT"]) && !empty($arItem["DISPLAY_PROPERTIES"]["IMAGE_1"]["FILE_VALUE"]["SRC"])) { ?>
                <div class="col-sm-6 col-lg-5">
                    <h2 class="h2"><?= $arItem["PROPERTIES"]["HEAD_1"]["VALUE"] ?></h2>
                    <p class="paragraf">
                        <?= $arItem["PROPERTIES"]["TEXT_1"]["~VALUE"]["TEXT"] ?>
                    </p>
                    <div class="img_wrap">
                        <img class="img" src="<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE_1"]["FILE_VALUE"]["SRC"] ?>" alt="">
                    </div>
                </div>
            <? } ?>
            <div class="col-sm-1"></div>
            <? if (!empty($arItem["DISPLAY_PROPERTIES"]["IMAGE_2"]["FILE_VALUE"]["SRC"]) && !empty($arItem["PROPERTIES"]["TEXT_2"]["~VALUE"]["TEXT"])) { ?>
                <div class="col-sm-5 col-lg-4">
                    <div class="img_wrap">
                        <img class="img" src="<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE_2"]["FILE_VALUE"]["SRC"] ?>" alt="">
                    </div>
                    <p class="paragraf">
                        <?= $arItem["PROPERTIES"]["TEXT_2"]["~VALUE"]["TEXT"] ?>
                    </p>
                </div>
            <? } ?>
        </div>
        <div class="row">
            <div class="col-12 col-lg-1"></div>
            <? if (!empty($arItem["PROPERTIES"]["HEAD_2"]["VALUE"]) && !empty($arItem["PROPERTIES"]["TEXT_3"]["VALUE"])) { ?>
                <div class="col-sm-6 col-lg-5">
                    <div class="h2"><?= $arItem["PROPERTIES"]["HEAD_2"]["VALUE"] ?></div>
                    <ul class="list">
                        <? foreach ($arItem["PROPERTIES"]["TEXT_3"]["VALUE"] as $list): ?>
                            <li class="list__item"><?= $list ?></li>
                        <? endforeach; ?>
                    </ul>
                </div>
            <? } ?>
            <? if (!empty($arItem["DISPLAY_PROPERTIES"]["IMAGE_3"]["FILE_VALUE"]["SRC"])) { ?>
                <div class="col-sm-6 col-lg-5">
                    <div class="img_wrap">
                        <img class="img" src="<?= $arItem["DISPLAY_PROPERTIES"]["IMAGE_3"]["FILE_VALUE"]["SRC"] ?>" alt="">
                    </div>
                </div>
            <? } ?>
        </div>
    </div>
<? endforeach; ?>
