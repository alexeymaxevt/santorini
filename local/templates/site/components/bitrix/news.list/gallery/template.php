<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

Loc::loadLanguageFile(__FILE__);
?>
<div class="gallery">
    <div class="container">
        <div class="row">

            <div class="col-md-1">

                <div class="title_vert"><?= Loc::getMessage('NAME'); ?></div>

                <div class="pagination">
                    <div class="pagination__pages" id="pagination__pages">
                        <button class="n_page">02</button>
                    </div>
                    <div class="now_page" id="now_page">01</div>
                </div>

            </div>

            <div class="col-md-11">
                <div class="gallery__background bg_waves"></div>
                <div class="gallery_slider" id="gallery_slider">
                    <? foreach ($arResult["ITEMS"] as $arItem): ?>
                        <? if (!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
                            <div class="glallery_slider__slide">
                                <? //Добавляем проверку на заполненность?>
                                <? //Добавляем обрезку картинок?>
                                <?
                                $renderImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], array("width" => 1280, "height" => 700), BX_RESIZE_IMAGE_PROPORTIONAL, false);
                                ?>
                                <img src="<?= $renderImage["src"] ?>" alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                     class="img">
                            </div>
                        <? } ?>
                    <? endforeach; ?>
                </div>
                <div class="gallery_slider_page mobile_only" id="gallery_slider_page">01</div>
            </div>

        </div>
    </div>
</div>
