<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?//Добавляем общий класс для обработки всех форм через аякс, и на него вешаем событие
//Так же файл который обрабатывает форму должен быть указан в атрибуте action тега form?>
<button class="btn_close" id="btn_close_main_form" type="button">
    <div class="line"></div>
</button>
<form class="container form ajax_form" action="/ajax/ajax_form.php">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-4">
            <div class="input_wrap"><input class="input" name="name" type="text" placeholder="Имя" required></div>
        </div>
        <div class="col-sm-4">
            <div class="input_wrap"><input class="input" name="email" type="email" placeholder="E-Mail" required></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
            <div class="input_wrap">
                <textarea class="textarea input" name="message" placeholder="Текст сообщения" required></textarea>
            </div>
            <div class="checkbox">
                <input type="checkbox" name="politics" id="politic" required>
                <label class="label" for="politic">Я согласен с правилами обработки</label>
            </div>
            <button type="submit" class="btn_orange">отправить</button>
        </div>
    </div>
</form>
<?//Переносим подключение файла в футер, и в этом файле пишем весь необходимый js?>
