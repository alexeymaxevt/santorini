<div class="row">
    <div class="col-12 col-lg-1"></div>
    <div class="col-sm-12 col-lg-11 capacity_block">
        <div class="h2">Вместимость</div>
        <div class="capacity">
            <div class="capacity__item">
                <div class="value">
                    <div class="val">15</div>
                    <div class="name">человек</div>
                </div>
                <div class="description">
                    При формате отдыха с персональными шезлонгами и банкетным столом
                </div>
            </div>
            <div class="capacity__item">
                <div class="value">
                    <div class="val">30</div>
                    <div class="name">человек</div>
                </div>
                <div class="description">
                    При формате отдыха с  фуршетным столом
                </div>
            </div>
            <div class="capacity__item">
                <div class="value">
                    <div class="val">40</div>
                    <div class="name">человек</div>
                </div>
                <div class="description">
                    При формате отдыха с банкетными столами без расстановки шезлонгов
                </div>
            </div>
        </div>
    </div>
</div>