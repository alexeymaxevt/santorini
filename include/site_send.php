<?
require_once($_SERVER['DOCUMENT_ROOT']. "/bitrix/modules/main/include/prolog_before.php");
//Тут пишем обработку ошибок в формате json
//Так же сохранение данных в инфоблок и в Модуль Веб-формы
if($_REQUEST['get'] == 'send')
{
    $error = '';
    if($_REQUEST['name']=='') {
        $error .="Заполните поле \"имя\"<br />";
    }
    if($_REQUEST['email']=='') {
        $error .="Заполните поле \"email\"<br />";
    }
    if($_REQUEST['message']=='') {
        $error .="Заполните текст сообщения<br />";
    }
    if($error)
        echo "<span style='color:#ff0000;'>".$error."</span>";
    else
    {
        $arEventFields = array(
            "NAME"      => $_REQUEST['name'],
            "EMAIL"      => $_REQUEST['email'],
            "MESSAGE"     => $_REQUEST['message'],
        );
        if(CEvent::Send("FEEDBACK_SEND", array(SITE_ID), $arEventFields))
            echo "<span style='color:#0fcf00;'>Сообщение успешно отправлено!</span>";
        else
            echo "<span style='color:#ff0000;'>Ошибка почтового сервера!</span>";
        CModule::IncludeModule("form");
        if (CModule::IncludeModule("form")) {
            $form_id = 1;
            $values = array(
                "form_text_1" => $_REQUEST['name'],
                "form_email_2" => $_REQUEST['email'],
                "form_textarea_3" => $_REQUEST['message'],
                "form_checkbox_4" => $_REQUEST["politics"]
            );
            $RESULT_ID = CFormResult::Add($form_id, $values);
        }
    }

}
?>