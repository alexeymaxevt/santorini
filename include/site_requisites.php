<div class="requisites">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-1"></div>
            <div class="col-12 col-lg-10">
                <div class="d-flex justify-content-between text-center">
                    <div class="requisites__col">
                        <div class="requisites__item">ИНН: 7716813200</div>
                        <div class="requisites__item">БИК: 044030723</div>
                    </div>
                    <div class="requisites__col">
                        <div class="requisites__item">Корр. счёт: 30101810100000000723</div>
                        <div class="requisites__item">Рас. счет: 40702810403000421433</div>
                    </div>
                    <div class="requisites__col">
                        <div class="requisites__item">ОГРН: 5157746208417</div>
                        <div class="requisites__item">КПП: 771601001</div>
                    </div>
                    <div class="requisites__col">
                        <div class="requisites__item">ОКПО: 52664241</div>
                        <div class="requisites__item">ОКТМО: 45351000000</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>